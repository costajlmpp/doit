package org.costajlmpp.business.reminders;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.json.JsonObject;
import javax.websocket.ClientEndpointConfig;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.WebSocketContainer;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;

public class ToDoChangesTrackerIT {

    private WebSocketContainer container;
    private ChangesListener listener;


    @Before
    public void initConteiner() throws URISyntaxException, IOException, DeploymentException {
        this.container = ContainerProvider.getWebSocketContainer();

        URI uri = new URI("ws://localhost:8080/doit/changes");
        this.listener = new ChangesListener();

        ClientEndpointConfig cec = ClientEndpointConfig.Builder.
                create().
                decoders(Collections.singletonList(JsonDecoder.class)).
                build();

        //this.container.connectToServer(this.listener, uri);
        this.container.connectToServer(this.listener, cec, uri);
    }

    @Test
    public void receiveNotifications() throws InterruptedException {
        JsonObject message = this.listener.getMessage();

        Assert.assertNotNull(message);

        System.out.println("---> " + message);
    }



}
