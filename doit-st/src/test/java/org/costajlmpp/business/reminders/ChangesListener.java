package org.costajlmpp.business.reminders;


import javax.json.JsonObject;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class ChangesListener extends javax.websocket.Endpoint {

    JsonObject message;
    CountDownLatch latch = new CountDownLatch(1);

    @Override
    public void onOpen(Session session, EndpointConfig endpointConfig) {
        /*
        session.addMessageHandler(String.class, new MessageHandler.Whole<String>() {

            @Override
            public void onMessage(String msg) {
                message = msg;
                latch.countDown();
                System.out.println("MSG:: " + msg);
            }
        });
         */

        session.addMessageHandler(JsonObject.class, new MessageHandler.Whole<JsonObject>() {

            @Override
            public void onMessage(JsonObject msg) {
                message = msg;
                latch.countDown();
                System.out.println("MSG:: " + msg);
            }
        });
    }

    public JsonObject getMessage() throws InterruptedException {
        latch.await(1L, TimeUnit.MINUTES);
        return message;
    }
}
