package org.costajlmpp.business.reminders;

import org.costajlmpp.business.JAXRSClientProvider;
import org.junit.Rule;
import org.junit.Test;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

import static org.costajlmpp.business.JAXRSClientProvider.buildWithURI;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class TodosResourceIT {

    @Rule
    public JAXRSClientProvider provider = buildWithURI("http://localhost:8080/doit/resources/todos");

    @Test
    public void crud() {
        System.out.println("--- CREATE TODO:: create To Do With High Priority Without Description---");
        createToDoWithHighPriorityWithoutDescription();

        System.out.println("--- CREATE TODO:: Without Caption---");
        createToDoWithoutCaption();

       System.out.println("--- CREATE TODO:: With Caption less than 3---");
       createToDoWithCaptionLessThan3();


        System.out.println("--- CREATE TODO ---");
        // POST
        URI location = this.createTodo();


        // find by id
        System.out.println("--- FIND TODO ---");
        System.out.println(location);
        JsonObject todo = getById(location);
        System.out.println(todo);

        // updates
        System.out.println("--- UPDATE TODO ---");
        updates(location, "Solid", 0);


        // updates status
        System.out.println("--- UPDATE TODO STATUS TO Done = true ---");
        updateTodoStatus(location, true);


        System.out.println("--- UPDATE STATUS OF NONEXISTENT TODO ---");
        updateStatusOfNonexistentTodo();

        System.out.println("--- MALFORMED STATUS UPDATE ---");
        malformedStatusUpdate(location);

        // Get ALL
        System.out.println("--- GET ALL TODOS ---");
        getAll();


        // update Again
        System.out.println("--- UPDATE WITH CONFLICT ---");
        updatesWithConflict(location, "Solid and TDD");

        // delete
        System.out.println("--- DELETE ---");
        delete();
    }

    private void updatesWithConflict(URI location, String description) {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        JsonObject todoToUpdate = builder.
                add("caption", "Implemented thing simple").
                add("description", description).
                add("priority", 90).
                add("version", 0).
                build();

        Response response = this.provider.client().
                target(location).
                request(MediaType.APPLICATION_JSON).
                put(Entity.json(todoToUpdate));

        assertThat(response.getStatus(), is(Response.Status.CONFLICT.getStatusCode()));
        assertTrue(response.getHeaderString("cause").startsWith("conflict occurred:"));
        System.out.println(response.getHeaderString("cause"));
    }

    private void malformedStatusUpdate(URI location) {

        JsonObjectBuilder builder = Json.createObjectBuilder();
        JsonObject status = builder.
                add("done_invalid", true).
                build();

        Response response = this.provider.client().
                target(location).
                path("status").
                request(MediaType.APPLICATION_JSON).
                put(Entity.json(status));

        assertThat(response.getStatus(), is(Response.Status.BAD_REQUEST.getStatusCode()));
        assertThat(response.getHeaderString("reason"), is("status json should contains field done"));
    }

    private void delete() {
        Response response = this.provider.target().
                path("23").
                request(MediaType.APPLICATION_JSON).
                delete();

        assertThat(response.getStatus(), is(204));
    }

    private void updates(URI location, String description, int version) {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        JsonObject todoToUpdate = builder.
                add("caption", "Implemented thing simple").
                add("description", description).
                add("priority", 90).
                add("version", version).
                build();

        Response updateResponse = this.provider.client().
                target(location).
                request(MediaType.APPLICATION_JSON).
                put(Entity.json(todoToUpdate));

        assertThat(updateResponse.getStatus(), is(Response.Status.NO_CONTENT.getStatusCode()));
    }

    private void getAll() {
        Response response = this.provider.target().
                request(MediaType.APPLICATION_JSON).
                get();

        assertThat(response.getStatus(), is(200));
        final JsonArray payload = response.readEntity(JsonArray.class);
        System.out.println(payload);
        assertFalse(payload.isEmpty());
        JsonObject firstOne = payload.getJsonObject(0);
        //assertTrue(firstOne.getString("caption").contains("Implement ABCDE"));
    }

    private void updateStatusOfNonexistentTodo() {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        JsonObject status = builder.
                add("done", true).
                build();

        Response response = this.provider.target().
                path("-1").
                path("status").
                request(MediaType.APPLICATION_JSON).
                put(Entity.json(status));

        assertThat(response.getStatus(), is(Response.Status.BAD_REQUEST.getStatusCode()));
        assertThat(response.getHeaderString("reason"), is("todo with id -1 does not exist"));
        // "reason", "todo with id " + id + " does not exist"
    }

    private void updateTodoStatus(URI location, boolean done) {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        JsonObject status = builder.
                add("done", done).
                build();

        Response updatStatuseResponse = this.provider.client().
                target(location).
                path("status").
                request(MediaType.APPLICATION_JSON).
                put(Entity.json(status));

        assertThat(updatStatuseResponse.getStatus(), is(Response.Status.OK.getStatusCode()));
        JsonObject updatedStatusTodo = updatStatuseResponse.readEntity(JsonObject.class);
        assertNotNull(updatedStatusTodo);
        assertThat(updatedStatusTodo.getBoolean("done"), is(done));
    }

    private JsonObject getById(URI location) {
        JsonObject todo = this.provider.client().
                target(location).
                request(MediaType.APPLICATION_JSON).
                get(JsonObject.class);

        assertNotNull(todo);
        assertTrue(todo.getString("caption").contains("Implement"));
        assertThat(todo.getInt("priority"), is(42));

        return todo;
    }

    private URI createTodo() {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        JsonObject todoToCreate = builder.
                add("caption", "Implement ABCDE").
                add("priority", 42).
                add("description", "doing all until tomorrow").
                build();



        Response postResponse = this.provider.target().request().post(Entity.json(todoToCreate));
        assertThat(postResponse.getStatus(), is(Response.Status.CREATED.getStatusCode()));
        URI location = postResponse.getLocation();
        System.out.println(location);
        assertNotNull(location);

        return location;
    }

    private void createToDoWithoutCaption() {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        JsonObject todoToCreate = builder.
                //add("caption", "Implement ABCDE").
                add("priority", 42).
                add("description", "doing all until tomorrow").
                build();

        Response response = this.provider.target().
                request().
                post(Entity.json(todoToCreate));


        assertThat(response.getStatus(), is(Response.Status.BAD_REQUEST.getStatusCode()));

        assertThat(response.getStatus(), is(Response.Status.BAD_REQUEST.getStatusCode()));
        assertTrue(response.getHeaderString("validation-exception").equals("true"));

        response.getHeaders().
                entrySet().
                forEach(e -> System.out.println(e.getKey() + ": " + e.getValue()));
        System.out.println(">>");
        JsonArray errors = response.readEntity(JsonArray.class);
        System.out.println(errors);
        System.out.println(">>");
    }

    private void createToDoWithCaptionLessThan3() {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        JsonObject todoToCreate = builder.
                        add("caption", "ab").
                        add("priority", 42).
                        add("description", "doing all until tomorrow").
                        build();

        Response response = this.provider.target().
                request().
                post(Entity.json(todoToCreate));


        assertThat(response.getStatus(), is(Response.Status.BAD_REQUEST.getStatusCode()));

        assertThat(response.getStatus(), is(Response.Status.BAD_REQUEST.getStatusCode()));
        assertTrue(response.getHeaderString("validation-exception").equals("true"));
        // 400
        // validation-exception -> true

        response.getHeaders().
                entrySet().
                forEach(e -> System.out.println(e.getKey() + ": " + e.getValue()));

        System.out.println(">>");
        JsonArray errors = response.readEntity(JsonArray.class);
        System.out.println(errors);
        System.out.println(">>");
    }

    public void createToDoWithHighPriorityWithoutDescription() {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        JsonObject todoToCreate = builder.
                add("caption", "This Should Be faild").
                add("priority", 11).
               // add("description", "doing all until tomorrow").
                build();

        Response response = this.provider.target().
                request().
                post(Entity.json(todoToCreate));


        assertThat(response.getStatus(), is(Response.Status.BAD_REQUEST.getStatusCode()));

        assertThat(response.getStatus(), is(Response.Status.BAD_REQUEST.getStatusCode()));
        assertTrue(response.getHeaderString("validation-exception").equals("true"));
        // 400
        // validation-exception -> true

        response.getHeaders().
                entrySet().
                forEach(e -> System.out.println(e.getKey() + ": " + e.getValue()));

        System.out.println(">>");
        JsonArray errors = response.readEntity(JsonArray.class);
        System.out.println(errors);
        System.out.println(">>");

    }


}
