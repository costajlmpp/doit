package org.costajlmpp.presentation;


import org.costajlmpp.business.reminders.boundary.ToDoManager;
import org.costajlmpp.business.reminders.entity.ToDo;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;

@Model
public class Index {

    @Inject
    Validator validator;

    @Inject
    ToDoManager manager;

    private ToDo todo;

    @PostConstruct
    void init() {
        this.todo = new ToDo();
    }

    public Object save() {
        Set<ConstraintViolation<ToDo>> violations = this.validator.validate(this.todo);

        if (violations.isEmpty()) {
            this.manager.save(todo);
        } else {
            violations.stream().
                    map(ConstraintViolation::getMessage).
                    forEachOrdered(this::showValidationError);
        }

        return "index";
    }

    public void showValidationError(String context) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, context, context);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.getExternalContext().getFlash().setKeepMessages(true);
        facesContext.addMessage("", message);
    }

    public void setTodo(ToDo todo) {
        this.todo = todo;
    }

    public ToDo getTodo() {
        return todo;
    }

    public List<ToDo> getTodos() {
        return this.manager.search();
    }
}
