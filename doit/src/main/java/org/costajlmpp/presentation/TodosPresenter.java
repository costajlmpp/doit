package org.costajlmpp.presentation;

import org.costajlmpp.business.reminders.boundary.ToDoManager;
import org.costajlmpp.business.reminders.entity.ToDo;

import javax.enterprise.inject.Model;
import javax.inject.Inject;
import java.util.List;

@Model
public class TodosPresenter {

    @Inject
    ToDoManager manager;

    public List<ToDo> getTodos() {
        return this.manager.search();
    }
}
