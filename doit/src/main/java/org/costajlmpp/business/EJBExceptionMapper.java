package org.costajlmpp.business;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.persistence.OptimisticLockException;
import javax.validation.ValidationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class EJBExceptionMapper implements ExceptionMapper<EJBException> {

    @Inject
    ValidationExceptionMapper validationMapper;

    @Override
    public Response toResponse(EJBException ex) {
        Throwable cause = ex.getCause();

        Response unknownError = Response.serverError().
                header("cause", ex.toString()).
                build();

        if (cause == null) {
            return unknownError;
        }

        if (cause instanceof OptimisticLockException ) {
            return Response.status(Response.Status.CONFLICT).
                    header("cause", "conflict occurred: " + cause).
                    build();
        }

        if (cause instanceof javax.validation.ValidationException) {
            return validationMapper.toResponse((ValidationException) cause);
        }


        return unknownError;
    }
}
