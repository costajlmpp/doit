package org.costajlmpp.business.monitor.boundary;

import org.costajlmpp.business.monitor.entity.CallSink;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Stateless
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Path("boundary-invocations")
public class BoundaryInvocationResources {

    @Inject
    Monitor monitor;

    @GET
    public List<CallSink> getMonitor() {
        return monitor.getRecentEvents();
    }
}
