package org.costajlmpp.business.monitor.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CallSink {

    private String method;
    private String className;
    private long duration;

    CallSink() {}

    public CallSink(String className, String method, long duration) {
        this.method = method;
        this.className = className;
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "CallSink{" +
                "method='" + method + '\'' +
                ", className='" + className + '\'' +
                ", duration=" + duration +
                '}';
    }

    public String getMethod() {
        return method;
    }

    public String getClassName() {
        return className;
    }

    public long getDuration() {
        return duration;
    }
}
