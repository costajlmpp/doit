package org.costajlmpp.business.monitor.boundary;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.LongSummaryStatistics;

@Stateless
@Path("boundary-statistics")
@Produces({MediaType.APPLICATION_JSON})
public class BoundaryStatisticsResource {

    @Inject Monitor monitor;

    @GET
    public JsonObject JsonObject() {
        LongSummaryStatistics statistics = monitor.getStatistics();
        return Json.createObjectBuilder().
                add("average", statistics.getAverage()).
                add("invocation-count", statistics.getCount()).
                add("min-duration", statistics.getMin()).
                add("max-duration", statistics.getMax()).
                build();
    }

}
