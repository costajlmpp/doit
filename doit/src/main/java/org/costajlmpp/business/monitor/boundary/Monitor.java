package org.costajlmpp.business.monitor.boundary;

import org.costajlmpp.business.monitor.entity.CallSink;

import javax.annotation.PostConstruct;
import javax.ejb.Asynchronous;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class Monitor {

    @Inject
    Logger logger;

    private CopyOnWriteArrayList<CallSink> events;

    @PostConstruct
    void initialize() {
        this.events = new CopyOnWriteArrayList<>();
    }

    @Asynchronous
    public void monitor(@Observes CallSink callSink) {
        logger.info(callSink.toString());
        this.events.add(callSink);
    }

    public List<CallSink> getRecentEvents() {
        return events;
    }

    public LongSummaryStatistics getStatistics() {
        return this.events.stream().
                collect(Collectors.summarizingLong(CallSink::getDuration));
    }

}
