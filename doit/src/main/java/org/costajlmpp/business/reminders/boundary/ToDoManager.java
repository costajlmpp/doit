package org.costajlmpp.business.reminders.boundary;

import org.costajlmpp.business.audit.boundary.Auditory;
import org.costajlmpp.business.reminders.entity.ToDo;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Auditory
@Stateless
public class ToDoManager {

    @PersistenceContext
    EntityManager em;

    public List<ToDo> search() {
        return this.em.createNamedQuery(ToDo.FIND_ALL, ToDo.class).getResultList();
    }

    public ToDo findById(long id) {
        return this.em.find(ToDo.class, id);
    }

    public ToDo save(ToDo toDo) {
        return this.em.merge(toDo);
    }

    public void delete(long id) {
        try {
            ToDo reference = this.em.getReference(ToDo.class, id);
            this.em.remove(reference);
        } catch (EntityNotFoundException e) {
            // no reference to delete
        }
    }

    public ToDo updateStatus(long id, boolean done) {
        ToDo toDo = this.findById(id);

        if (toDo == null) {
            return null;
        }

        toDo.setDone(done);
        return toDo;
    }
}
