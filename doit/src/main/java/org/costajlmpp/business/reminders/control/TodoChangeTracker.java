package org.costajlmpp.business.reminders.control;

import org.costajlmpp.business.reminders.entity.ToDo;
import org.costajlmpp.business.reminders.entity.ToDoSnapshot;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;

@ApplicationScoped
public class TodoChangeTracker {
// (during = TransactionPhase.AFTER_SUCCESS)
    public void onTodoChanges(@Observes(during = TransactionPhase.AFTER_SUCCESS) ToDoSnapshot todo) {
        System.out.println("#### Todo Change and commited !!! " + todo );
    }

    public void onTodoChanges(@Observes(during = TransactionPhase.AFTER_SUCCESS) ToDo todo) {
        System.out.println("#### Todo Change and commited !!! " + todo );
    }

}
