package org.costajlmpp.business.reminders.boundary;

import org.costajlmpp.business.reminders.entity.ToDo;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

@Stateless
@Path("todos")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class ToDosResource {

    @Inject
    ToDoManager manager;

    @Path("{id}")
    public TodoResource find(@PathParam("id") long id) {
        return new TodoResource(id, this.manager);
    }

    @GET
    public List<ToDo> all() {
        return this.manager.search();
    }

    @POST
    public Response save (@Valid ToDo toDo, @Context UriInfo info) {
        System.out.println("----- On create ---- ");
        ToDo saved = this.manager.save(toDo);
        URI uri = info.getAbsolutePathBuilder().path("{id}").build(saved.getId());
        return Response.created(uri).entity(saved).build();
    }
}
