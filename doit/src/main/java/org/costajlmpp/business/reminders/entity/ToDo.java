package org.costajlmpp.business.reminders.entity;

import org.costajlmpp.business.validation.CrossCheck;
import org.costajlmpp.business.validation.ValidEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@CrossCheck(message = "{org.costajlmpp.business.reminders.entity.ToDo.CrossCheck.message}")
@Entity
@SequenceGenerator(name="ToDo_SEQ", allocationSize=100)
@NamedQuery(name = ToDo.FIND_ALL, query = "select t from ToDo t order by t.id")
@EntityListeners(ToDoAuditor.class)
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ToDo implements ValidEntity {

    private static final String PREFIX = "business.reminders.entity.";
    public static final String FIND_ALL = PREFIX + "find_all";

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ToDo_SEQ")
    private long id;

    @NotNull
    @Size(min = 3, max = 250)
    private String caption;

    private String description;
    private int priority;
    private boolean done;

    @Version
    private int version;

    public ToDo(String caption, String description, int priority) {
        this.caption = caption;
        this.description = description;
        this.priority = priority;
    }

    public ToDo() {}

    public String getCaption() {
        return caption;
    }

    public String getDescription() {
        return description;
    }

    public int getPriority() {
        return priority;
    }

    public long getId() {
        return id;
    }

    public boolean isDone() {
        return done;
    }

    public int getVersion() {
        return version;
    }

    // TODO:: check if it is really necessary to have set methods

    public void setId(long id) {
        this.id = id;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public boolean isValid() {
        if (priority <= 10) {
            return true;
        }

        return !(this.description == null || this.description.trim().isEmpty());
    }
}

