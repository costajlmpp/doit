package org.costajlmpp.business.reminders.boundary;

import org.costajlmpp.business.encoders.JsonEncoder;
import org.costajlmpp.business.reminders.entity.ToDo;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.json.Json;
import javax.json.JsonObject;
import javax.websocket.EncodeException;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@ServerEndpoint(value = "/changes", encoders = {JsonEncoder.class})
public class ToDoChangesTracker {

    private Session session;

    @OnOpen
    public void onOpen(Session session) {
        this.session = session;
    }

    public void onClose() {
        this.session = null;
    }

    public void onToDoCreation(
            @Observes(during = TransactionPhase.AFTER_SUCCESS)
                    @ChangeEvent(ChangeEvent.Type.CREATION)
                    ToDo todo) {
         sendMessage(todo, "creation");
    }


    public void onToDoUpdates(@Observes(during = TransactionPhase.AFTER_SUCCESS)
                              @ChangeEvent(ChangeEvent.Type.UPDATE)
                              ToDo todo) {
        sendMessage(todo, "update");
    }

    private void sendMessage(ToDo todo, String cause) {
        if (this.session != null && this.session.isOpen()) {
            try {

                JsonObject message = Json.createObjectBuilder().
                        add("id", todo.getId()).
                        add("done", todo.isDone()).
                        add("cause", cause).
                        add("caption", todo.getCaption()).
                        build();

                this.session.getBasicRemote().sendObject(message);
            } catch (IOException e) {
                // ignored
            } catch (EncodeException e) {
                e.printStackTrace();
            }
        }
    }

}
