package org.costajlmpp.business.reminders.boundary;

import org.costajlmpp.business.reminders.entity.ToDo;

import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class TodoResource {

    private final Long id;
    private final ToDoManager manager;

    public TodoResource(Long id, ToDoManager manager) {
        this.id = id;
        this.manager = manager;
    }

    @GET
    //@Path("{id}")
    public ToDo find() {
        return this.manager.findById(id);
    }


    @DELETE
    public void delete() {
        this.manager.delete(id);
    }

    @PUT
   // @Path("{id}")
    public void statusUpdate(ToDo todo) {
        todo.setId(id);
        this.manager.save(todo);
    }

    @PUT
    @Path("status")
    public Response update(JsonObject status) {

        if (status == null) {
            return Response.status(Response.Status.BAD_REQUEST).
                    header("reason", "status json must be informed in the request").
                    build();
        }

        if (!status.containsKey("done")) {
            return Response.status(Response.Status.BAD_REQUEST).
                    header("reason", "status json should contains field done").
                    build();
        }

        boolean done = status.getBoolean("done");
        ToDo toDo = this.manager.updateStatus(id, done);

        if (toDo == null ) {
            return Response.status(Response.Status.BAD_REQUEST).
                    header("reason", "todo with id " + id + " does not exist").
                    build();
        }

        return Response.ok(toDo).build();
    }
}
