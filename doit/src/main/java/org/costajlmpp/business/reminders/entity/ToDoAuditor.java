package org.costajlmpp.business.reminders.entity;

import org.costajlmpp.business.reminders.boundary.ChangeEvent;

import javax.enterprise.event.Event;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

public class ToDoAuditor {

    @Inject
    @ChangeEvent(ChangeEvent.Type.CREATION)
    Event<ToDo> creationEvent;

    @Inject
    @ChangeEvent(ChangeEvent.Type.UPDATE)
    Event<ToDo> updateEvent;

    BeanManager beanManager = CDI.current().getBeanManager();

    @PostPersist
    public void onCreation(ToDo todo) {
        System.out.println("TODO:: this will be perfect In Java EE 8 because will have CDI support!!!");
        System.out.println("TODO:: Ok, you can thing that we can use the Bean Manager, but acording my the test," +
                " current the events fire by the Bean Manager and not be observed the Transactions phases.");

        System.out.println("TODO:: But the cool thing is that the Payara Application server can handler EntityListeners " +
                "as CDI Manager beans!");


        this.creationEvent.fire(todo);

        //beanManager.fireEvent(todo);
    }

    @PostUpdate
    public void onUpdate(ToDo todo) {
        System.out.println("TODO:: this will be perfect In Java EE 8 because will have CDI support!!!");
        System.out.println("TODO:: Ok, you can thing that we can use the Bean Manager, but acording my the test," +
                " current the events fire by the Bean Manager and not be observed the Transactions phases.");

        System.out.println("TODO:: But the cool thing is that the Payara Application server can handler EntityListeners " +
                "as CDI Manager beans!");


        this.updateEvent.fire(todo);

        //beanManager.fireEvent(todo);
    }
        
}
