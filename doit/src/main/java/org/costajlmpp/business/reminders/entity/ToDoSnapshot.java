package org.costajlmpp.business.reminders.entity;

import java.io.Serializable;

public class ToDoSnapshot implements Serializable {

    private ToDo toDo;

    public ToDoSnapshot(ToDo toDo) {
        this.toDo = toDo;
    }

    public ToDoSnapshot() {
    }


    public ToDo getToDo() {
        return toDo;
    }
}
