package org.costajlmpp.business.audit.boundary;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import java.util.logging.Logger;

public class LoggerProducer {

    @Produces
    public Logger criaLogger(InjectionPoint ip){
        Class<?> injectionTarget = ip.getMember().getDeclaringClass();
        final String className = injectionTarget.getName();
        return Logger.getLogger(className);
    }
}
