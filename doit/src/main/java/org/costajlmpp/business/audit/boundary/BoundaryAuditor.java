package org.costajlmpp.business.audit.boundary;

import org.costajlmpp.business.monitor.entity.CallSink;

import javax.annotation.Priority;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

@Auditory
@Interceptor @Priority(Interceptor.Priority.APPLICATION)
public class BoundaryAuditor {

   @Inject
   Logger logger;

   @Inject
   Event<CallSink> callSinkEvent;

    @AroundInvoke
    public Object audit(InvocationContext ic) throws Exception {

        Object target = ic.getTarget();
        Method method = ic.getMethod();
        Class<?> aClass = target.getClass();
        String aClassName = aClass.getName();
        String methodName = method.getName();

        long time = System.nanoTime();

        try {

            return ic.proceed();

        } finally {


            long durations = TimeUnit.MILLISECONDS.convert(System.nanoTime() - time, TimeUnit.NANOSECONDS);
            logger.info("--- " + aClassName + "#" + methodName + " --- " + durations + "ms");

            this.callSinkEvent.fire(new CallSink( aClassName, methodName, durations));

        }
    }
}
