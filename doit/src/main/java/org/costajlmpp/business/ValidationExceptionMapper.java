package org.costajlmpp.business;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import javax.validation.ValidationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Provider
public class ValidationExceptionMapper implements ExceptionMapper<ValidationException> {

    /*
     * NOTE: https://jaxenter.com/integrating-bean-validation-with-jax-rs-2-106887.html
     */

    @Override
    public Response toResponse(ValidationException e) {

        if (e instanceof  ConstraintViolationException) {
            ConstraintViolationException constraintViolationException = (ConstraintViolationException) e;
            final List<ValidationError> errors = constraintViolationException.getConstraintViolations().
                    stream().
                    map(this::toValidationError).
                    collect(Collectors.toList());
            //Validation.VALIDATION_HEADER;
            return Response.status(Response.Status.BAD_REQUEST).
                    header("validation-exception", true).
                    entity(errors).
                    type(MediaType.APPLICATION_JSON).
                    build();
        }

        return Response.serverError().
                header("validation-exception", true).
                type(MediaType.APPLICATION_JSON).
                build();
    }

    private ValidationError toValidationError(ConstraintViolation constraintViolation) {
        final Path propertyPath = constraintViolation.getPropertyPath();

        Iterator<Path.Node> iterator = propertyPath.iterator();
        if(iterator.hasNext()) {
            //Path.Node next = iterator.next();
            //String name = next.getName();

            String beanName = null;
            String propertyName = null;
            String methodName = null;
            String parameterName = null;

            for (; iterator.hasNext(); ) {
                Path.Node o = iterator.next();

                switch (o.getKind()) {
                    case BEAN:
                        beanName = o.getName();
                        break;
                    case PROPERTY:
                        propertyName = o.getName();
                        break;
                    case METHOD:
                        methodName = o.getName();
                        break;
                    case PARAMETER:
                        parameterName = o.getName();
                        break;
                }
            }

            if (propertyName != null) {
                return new ValidationError(
                        constraintViolation.getPropertyPath().toString(),
                        propertyName + " " + constraintViolation.getMessage());
            }

            return new ValidationError(
                    constraintViolation.getPropertyPath().toString(),
                    constraintViolation.getMessage());
        }

        return new ValidationError(
                constraintViolation.getPropertyPath().toString(),
                constraintViolation.getMessage());
    }

    @XmlRootElement
    @XmlAccessorType(XmlAccessType.FIELD)
    private static class ValidationError {
        private String path;
        private String message;

        public ValidationError(String path, String message) {
            this.path = path;
            this.message = message;
        }

        public ValidationError() {}

        public String getMessage() {
            return message;
        }

        public String getPath() {
            return path;
        }
    }
}
