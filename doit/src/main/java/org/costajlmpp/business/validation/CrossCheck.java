package org.costajlmpp.business.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = CrossCheckConstraintValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface CrossCheck {

    String message() default "{org.costajlmpp.business.reminders.CrossCheck.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /*
     * field name in the message
     * https://stackoverflow.com/questions/29827436/how-do-i-display-the-field-name-description-in-the-constraint-violation-message/29842167#29842167
     */
}
