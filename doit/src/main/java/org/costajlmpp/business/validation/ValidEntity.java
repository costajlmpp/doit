package org.costajlmpp.business.validation;

public interface ValidEntity {

    boolean isValid();
}
