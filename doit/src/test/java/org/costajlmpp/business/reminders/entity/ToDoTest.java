package org.costajlmpp.business.reminders.entity;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ToDoTest {
    @Test
    public void isValid() throws Exception {
        ToDo toDo = new ToDo("", "avariable", 10);
        assertTrue(toDo.isValid());
    }

    @Test
    public void isNotValid() throws Exception {
        ToDo toDo = new ToDo("", "", 11);
        assertFalse(toDo.isValid());
    }

    @Test
    public void todoWithoutDescription() {
        ToDo valid = new ToDo(null, null, 10);
        assertTrue(valid.isValid());
    }
}