#!/usr/bin/jjs -fv

/*
Next-Generation JavaScript Engine for the JVM
Oracle Nashorn - http://www.oracle.com/technetwork/articles/java/jf14-nashorn-2126515.html


TO Run this simple example execute in the terminal:
$ jjs -scripting all.js
 */
var uri = "http://localhost:8080/doit/resources/todos";
var command = "curl ${uri}";
print(command);
$EXEC(command);
var result = $OUT;
print(result);
var resultAsArray = JSON.parse(result);
print(resultAsArray);
for (index in resultAsArray) {
    print(resultAsArray[index].caption + " -- " + resultAsArray[index].description)
}

